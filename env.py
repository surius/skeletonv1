import pandas as pd
import numpy as np
import plotly as py
import plotly.graph_objs as go
import collections
import random
import ML_skeleton as ms

# Generate the list of states
# states = []
time_horizon = 120
total_shares = 1000
max_time_step = 4

tick = 100



class Agent(object):

    def __init__(self, env):

        self.env = env

        self.remainShares = total_shares
        self.inventory = env.inventory_lvl

    def update(self):
        pass


class Environment(object):
    """Environment within which agents operate."""

    def __init__(self, ask_book,bid_book,message_file):#verbose=False

        self.inventory_lvl = 4.0

        self.i = self.inventory_lvl
        self.done = False
        self.t = 0  # first submit at time 0
        # initiate agent
        self.agent = self.create_agent(Agent)
        # initiate state at time zero
        self.state = [self.t, self.i]
        self.each_i = 1.0
        self.ask_book=ask_book
        self.init_ask_book=ask_book
        self.bid_book=bid_book
        self.message_file=message_file
        self.num_act = 10
        self.time_point=34200

    def step(self, action):#,i_episode
        """ This function is called when a time step is taken turing a trial. """
        #max_time = self.time_step  # no. of time steps
        #max_level = self.inventory_lvl

        # state = self.state
        agent = self.agent
        t = self.t
        each_i = self.each_i
        reward = 0
        agent.price = self.best_bid_price

        if self.t == max_time_step:
            #ms.add_new_mkt_order(self.ask_book, time_point+t*30, agent.inventory,total_shares / self.inventory_lvl)
            my_mkt_list = ms.acts_for_market_orders(self.bid_book,'bid',agent.remainShares, self.time_point+t*30) #check exection in next 30s
            for k in my_mkt_list:
                reward += (k[0] - self.bid_ask_mid) * k[1]
            agent.remainShares = 0
            agent.inventory = 0
            self.done = True

            # set the start time of next episode
            self.time_point += 120
        else:
            agent.price = self.best_ask_price - action * tick
            ms.add_new_order_row(self.ask_book, self.time_point+t*30, agent.price, each_i,total_shares / self.inventory_lvl)
            new_ask_book,price_size_list = ms.acts_for_limit_orders(self.ask_book, 'ask',self.message_file,total_shares / self.inventory_lvl,self.time_point+t*30,self.time_point+(t+1)*30) # check my execution
            self.ask_book = new_ask_book
            if price_size_list:
                for k in price_size_list:
                    agent.remainShares -= k[1] # if no execution,changed_size=0
                    reward += (k[0] - self.bid_ask_mid) * k[1]
                    agent.inventory -= k[1] / (total_shares / self.inventory_lvl)
            self.t += 1

        self.i = agent.inventory

        next_state = [self.t, self.i]

        return next_state, reward, self.done



    def reset(self):
        """This function is called at the beginning of a new trial (episode)"""

        self.done = False
        self.t = 0
        self.i = 4.0
        self.ask_book = self.init_ask_book
        # initiate agent
        self.agent = self.create_agent(Agent)

        # initiate state at time zero
        self.state = [self.t, self.i]

        # check if mkt orders exist in this episode
        while True:
            df = ms.check_market_order_exsitence(self.message_file[1], self.time_point, self.time_point + 120)
            if df.empty:
                self.time_point += 120
            else:
                break

        # initiate best price in this episode
        self.best_ask_price = ms.get_best_price(self.time_point, 'ask', self.ask_book)
        self.best_bid_price = ms.get_best_price(self.time_point, 'bid', self.bid_book)
        self.bid_ask_mid = (self.best_ask_price + self.best_bid_price) / 2

        return self.state

    def create_agent(self, agent_class, *args, **kwargs):
        agent = agent_class(self, *args, **kwargs)
        return agent