# create on 10/3/2019

import random
import math

import numpy as np
import pandas as pd
from env import Agent, Environment
from collections import defaultdict, deque
import sys
import matplotlib.pyplot as plt
import ML_skeleton as ms
time_step = 4


class LearningAgent(Agent):
    # epsilon:random exploration factor
    def __init__(self,env,epsilon,alpha,q_table):
        super(LearningAgent, self).__init__(env)  # Set the agent in the environment
        #self.num_act = self.env.num_act
        self.actions = list(range(env.num_act))
        self.epsilon = epsilon  # Random exploration factor
        self.alpha = alpha # Learning rate
        self.gamma = 1
        if q_table.empty:
            self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)#initial q_table
        else:
            self.q_table = q_table

    def select_action(self,  observation,i_episode):
        self.epsilon = 1 / (i_episode + 1)
        #if i_episode>100:
        #    self.epsilon=1.0
        self.check_state_exist(observation)
        # output action to take for current state
        if(np.random.uniform() > self.epsilon):
            action = np.random.choice(self.actions)
        else:
            state_action = self.q_table.loc[observation, :]
            #print(state_action)
            # some actions may have the same value, randomly choose on in these actions
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        return action

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state
                )
            )

    # on-policy
    def Sarsa_learn(self, s, a, r, s_, a_):
        self.check_state_exist(s_)
        q_predict = self.q_table.loc[s, a]
        if s_[0] != 4:
            q_target = r + self.gamma * self.q_table.loc[s_, a_]  # next state is not terminal
        else:
            q_target = r  # next state is terminal
        self.q_table.loc[s, a] += self.alpha * (q_target - q_predict)  # update

    #off-policy
    def q_learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        q_predict = self.q_table.loc[s, a]
        if s_[0]!= 4:
            q_target = r + self.gamma * self.q_table.loc[s_, :].max()  # next state is not terminal
        else:
            q_target = r  # next state is terminal
        self.q_table.loc[s, a] += self.alpha * (q_target - q_predict)  # update

    def sarsa(self, env, num_episodes):#episode=141
        for i_episode in range(num_episodes + 1):
             # monitor progress
            #if (env.time_point > 57479):
             #    break
            if i_episode % 100 == 0:
                print("\rEpisode {}/{}".format(i_episode, num_episodes), end="")
                sys.stdout.flush()

            state = env.reset()
            action = self.select_action(str(state),i_episode) # turn list into string
            while True:
                next_state, reward, done = env.step(action)#, i_episode

                next_action = self.select_action(str(next_state),i_episode)

                self.Sarsa_learn(str(state), action, reward, str(next_state), next_action)

                state = next_state
                action = next_action
                if done:
                    break


        print(self.q_table)

        return self.q_table

    def q_learning(self, env, num_episodes):
        # initialize action-value function (empty dictionary of arrays)

        for i_episode in range(num_episodes + 1):
             # monitor progress

            if i_episode % 100 == 0:
                print("\rEpisode {}/{}".format(i_episode, num_episodes), end="")
                sys.stdout.flush()

            state = env.reset()


            while True:
                action = self.select_action(str(state), i_episode)  # turn list into string

                next_state, reward, done = env.step(action)#, i_episode

                self.q_learn(str(state), action, reward, str(next_state))

                state = next_state

                if done:
                    break

        print(self.q_table)

        return self.q_table


    def estimate_returns(self, testenv, Q, num_episodes):
        returns = []
        # initialize performance monitor
        # loop over episodes
        for i_episode in range(num_episodes + 1):

            state = testenv.reset()
            return_sum = 0
            while True:
                observation = str(state)
                #choose the best action
                state_action = Q.loc[observation, :]
                # some actions may have the same value, randomly choose on in these actions
                action = np.random.choice(state_action[state_action == np.max(state_action)].index)

                next_state, reward, done = testenv.step(float(action))#, i_episode
                return_sum += reward
                state = next_state
                if done:
                    break

            returns.append(return_sum)

        return returns

def run():
    #train the model
    #first time training

    ask_book, bid_book = ms.data_processing('CZR_2019-02-28_34200000_57600000_orderbook_5.csv')
    message_file = ms.message_file_sum('CZR_2019-02-28_34200000_57600000_message_5.csv')
    env = Environment(ask_book,bid_book,message_file)
    dummy_table = pd.DataFrame()
    agent = env.create_agent(LearningAgent, 0.9, 0.6,dummy_table) # epsilon,alpha
    Q_sarsa = agent.sarsa(env, 140)


    file_lists=[['CZR_2019-02-27_34200000_57600000_orderbook_5.csv',
                 'CZR_2019-02-27_34200000_57600000_message_5.csv'],
                ['CZR_2019-02-26_34200000_57600000_orderbook_5.csv',
                 'CZR_2019-02-26_34200000_57600000_message_5.csv']
                ] # list of file names [order book,message]
    book_lists=[] # list of list [ask book,bid book,message]
    for file in file_lists:
        ab,bb = ms.data_processing(str(file[0]))
        mf = ms.message_file_sum(str(file[1]))
        book_lists.append([ab,bb,mf])

    for book in book_lists:
        envi = Environment(book[0],book[1],book[2])
        agent = envi.create_agent(LearningAgent,0.9,0.6,Q_sarsa)
        tmp = agent.sarsa(envi,140)
        Q_sarsa = tmp
    return Q_sarsa
    #Q_learning = agent.q_learning(env, 100)


def test(q_table):
    ask_book, bid_book = ms.data_processing('CZR_2018-12-03_34200000_57600000_orderbook_5.csv')
    message_file = ms.message_file_sum('CZR_2018-12-03_34200000_57600000_message_5.csv')
    testenv = Environment(ask_book, bid_book, message_file)
    agent = testenv.create_agent(LearningAgent, 0.9, 0.6, q_table)  # epsilon,alpha

    #calculate the return of test data
    returns = agent.estimate_returns(testenv, q_table, 100)
    cumulative_returns = np.cumsum(returns)
    plt.plot(cumulative_returns)
    plt.show()
    average_return = (sum(returns) / len(returns))
    print('The average return following the optimal strategy is : $', average_return)

def submit_n_leave():
    ask_book, bid_book = ms.data_processing('CZR_2019-02-27_34200000_57600000_orderbook_5.csv')
    message_file = ms.message_file_sum('CZR_2019-02-27_34200000_57600000_message_5.csv')
    snl_env = Environment(ask_book, bid_book, message_file)

    num_episodes=100
    time_point=snl_env.time_point
    returns=[]

    for i_episode in range(num_episodes+1):
        reward=0
        agent = snl_env.create_agent(Agent)
        # check if mkt orders exist in this episode
        while True:
            df = ms.check_market_order_exsitence(message_file[1], time_point, time_point + 120)
            if df.empty:
                time_point += 120
            else:
                break

        #decide limit order price
        my_row = ask_book[ask_book['Time'] >= time_point].iloc[0]
        ask_price=my_row.loc[['ask_price_1','ask_price_2','ask_price_3','ask_price_4','ask_price_5'],]
        spread=ask_price['ask_price_2']-ask_price['ask_price_1']
        agent.price=random.randrange(ask_price['ask_price_1']+spread,ask_price['ask_price_2']+spread)

        # here agent.remainShares=total shares
        bid_ask_mid = (ms.get_best_price( time_point, 'ask', ask_book) + ms.get_best_price(time_point, 'bid', bid_book)) / 2
        ms.add_new_order_row(ask_book, time_point, agent.price, snl_env.inventory_lvl,
                             agent.remainShares / snl_env.inventory_lvl)

        new_ask_book, price_size_list = ms.acts_for_limit_orders(ask_book, 'ask', message_file,
                                                                 agent.remainShares / snl_env.inventory_lvl,
                                                                 time_point,
                                                                 time_point + 119)
        if price_size_list:
            for k in price_size_list:
                reward += (k[0] - bid_ask_mid) * k[1]
                agent.remainShares -= k[1]
        else: print("no mkt order in this period!")

        if agent.remainShares>0:
            my_mkt_list = ms.acts_for_market_orders(bid_book, 'bid', agent.remainShares,
                                                    time_point + 120)
            for k in my_mkt_list:
                reward += (k[0] - bid_ask_mid) * k[1]

        time_point+=120
        returns.append(reward)
    print(returns)
    #calculate the return
    cumulative_returns = np.cumsum(returns)
    plt.plot(cumulative_returns)
    plt.show()
    average_return = (sum(returns) / len(returns))
    print('The average return following the optimal strategy is : $', average_return)

if __name__ == '__main__':
    # res=run()
    # res.to_csv('q_table.csv', encoding='utf-8')
    # #res=pd.DataFrame(pd.read_csv('q_table.csv',index_col=0))
    # test(res)
    submit_n_leave()



