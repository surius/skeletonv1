

import pandas as pd
import operator
from copy import deepcopy
import numpy as np


def data_processing(path):
    '''
    process price_size data
    :param path:
    :return: df_ask, df_bid: ask book and bid book
    '''
    print('start processing raw order file data\n')
    df_main=pd.DataFrame(pd.read_csv(path,names=['Time',
                                                        'ask_price_1', 'ask_size_1',
                                                        'bid_price_1', 'bid_size_1',
                                                        'ask_price_2', 'ask_size_2',
                                                        'bid_price_2', 'bid_size_2',
                                                        'ask_price_3', 'ask_size_3',
                                                        'bid_price_3', 'bid_size_3',
                                                        'ask_price_4', 'ask_size_4',
                                                        'bid_price_4', 'bid_size_4',
                                                        'ask_price_5', 'ask_size_5',
                                                        'bid_price_5', 'bid_size_5',
                                                  ]))
    df_main['Time']=round(df_main['Time'],5)
    df_ask=df_main.iloc[:,[0,1,2,5,6,9,10,13,14,17,18]]
    df_ask['my_price'] = 0
    df_ask['my_size'] = 0
    df_ask.drop_duplicates('Time',keep='last', inplace=True)
    #df_bid=df_main.drop(df_main.columns[1:11], axis=1)
    df_bid=df_main.iloc[:,[0,3,4,7,8,11,12,15,16,19,20]]
    df_bid['my_price'] = 0
    df_bid['my_size'] = 0
    df_bid.drop_duplicates('Time',keep='last', inplace=True)
    return df_ask, df_bid


def message_file_sum(path):
    '''
    find the message that market orders appear (event_type==4)
    :param path:
    :return: df_order_bid, df_order_ask
    '''
    print('start processing raw message file\n ')
    df_sub=pd.DataFrame(pd.read_csv(path,names=['Time','Event_Type','Order Id','Size','Price','Direction','Info']),)
    df_order_bid_raw=df_sub.loc[(df_sub['Event_Type']== 4) & (df_sub['Direction']== 1)]
    df_order_bid_raw['Time']=round(df_order_bid_raw,5)
    df_order_ask_raw=df_sub.loc[(df_sub['Event_Type']== 4) & (df_sub['Direction']== -1)]
    df_order_ask_raw['Time']=round(df_order_ask_raw,5)
    df_order_bid=pd.DataFrame(df_order_bid_raw.groupby(['Time'])['Size'].sum())
    df_order_bid=df_order_bid.reset_index()
    df_order_ask=pd.DataFrame(df_order_ask_raw.groupby(['Time'])['Size'].sum())
    df_order_ask=df_order_ask.reset_index()
    df_order_ask.to_csv('message_ask.csv')
    df_cancel_order_ask_raw = df_sub.loc[((df_sub['Event_Type'] == 2) | (df_sub['Event_Type'] == 3)) & (df_sub['Direction'] == -1)]
    df_cancel_order_ask_raw['Time'] = round(df_cancel_order_ask_raw, 5)
    df_cancel_order_ask=df_cancel_order_ask_raw.reset_index()
    return df_order_bid, df_order_ask,df_cancel_order_ask


def add_new_order_row(dataframes,time_point,price, inventory, shares):
    '''
    add my order into the book dataframe.
    attention: dataframe can be ask book for limit orders and bid book for market orders
    :param dataframes:
    :param time_point:
    :param price:
    :param inventory: i in the paper
    :param shares: shares/ i
    :return: dataframes: new dataframes after adding my order.
    '''
    if time_point==34200:
        dataframes['my_price']=price
        dataframes['my_size']= inventory*shares
        print("successfully add new limit orders.\n")
        return dataframes
    else:
        dataframes_1 = dataframes[dataframes['Time'] < time_point]
        if str(time_point) not in dataframes['Time']:
            insert_point = len(dataframes_1)
            new_row = dataframes_1[-1:]
            # empty dataframes_1
            new_row.loc[0,'Time'] = time_point
            new_row.loc[0,'my_price'] = price
            new_row.loc[0,'my_size'] = inventory*shares
            new_follow_row = dataframes[insert_point:]
            new_follow_row['my_price'] = price
            new_follow_row['my_size'] = inventory*shares
            new_df = pd.concat([dataframes_1, new_row, new_follow_row]).reset_index(drop=True)
        else:
            new_df=dataframes[dataframes['Time'] >= time_point]
            new_df['my_price'] = price
            new_df['my_size'] = inventory*shares
        print("successfully add new limit orders.\n")
        return new_df


def delete_my_old_order(dataframes,time_point):
    '''
    delete my order before next new order, it's ok if u don't use this since next order will cover old orders.
    just in case.
    :param dataframes:
    :param time_point:
    :return: dataframes without last my order.
    '''
    dataframes[dataframes[dataframes['Time'] == time_point].index:].my_price = 0
    dataframes[dataframes[dataframes['Time'] == time_point].index:].my_size = 0
    return dataframes


def order_the_price(certain_row, book_class, new_price=-1):
    '''
    this part is used for ranking prices of a certain row when market order appear
    :param certain_row: only need one row before market order to decide the rank of prices
    :param book_class: for classify sorted type.
    :param new_price: another price which needs rank, used for update_following_rows
    :return:
    '''
    new_price = float(new_price)
    price_name_list=[]
    flag=0
    if book_class=='ask':
        certain_row_select=certain_row[['ask_price_1', 'ask_price_2', 'ask_price_3', 'ask_price_4', 'ask_price_5','my_price']]
        price_list= list(certain_row_select)
        if new_price !=-1:
            flag=1
            price_list.append(new_price)
        sorted_price=sorted(price_list)
        # acendence
    else:
        certain_row_select=certain_row[['bid_price_1', 'bid_price_2', 'bid_price_3', 'bid_price_4', 'bid_price_5','my_price']]
        price_list = list(certain_row_select)
        if new_price!=-1:
            flag=1
            price_list.append(float(new_price))
        sorted_price = sorted(price_list,reverse=True)
        # decendence
    if flag==0:
        for i in sorted_price:
            if certain_row_select[certain_row_select == i].index[0] in price_name_list:
                price_name_list.append('my_price')
            else:
                price_name_list.append(certain_row_select[certain_row_select == i].index[0])
    return sorted_price, price_name_list


def check_market_order_exsitence(message_file,start_time,end_time):
    '''
    check market orders in a certain period.
    :param message_file:
    :param start_time:
    :param end_time:
    :return: dataframe of message file which contain detailed market order info in this period.
    '''
    market_order=message_file.loc[(message_file['Time'] >= float(start_time)) &
                                  (message_file['Time'] < float(end_time))].reset_index(drop=True)
    if len(market_order)>0:
        print("There exist market orders\n")
    else:
        print("No market order from excel files\n.")
    return market_order


def get_best_price(time_point,book_class, dataframes):
    certain_row = dataframes[dataframes.Time == time_point]
    if len(certain_row)>0:
        price_list,price_name_list = order_the_price(certain_row,book_class)
    else:
        if time_point == 34200:
            following_row=dataframes[dataframes['Time'] >= time_point]
            certain_row=following_row.iloc[0]
        else:
            previous_row=dataframes[dataframes['Time'] <= time_point]
            certain_row=previous_row.iloc[-1]
        price_list, price_name_list= order_the_price(certain_row,book_class)
    for i in price_list:
        if i !=0:
            return float(i)
    print("something wrong here")
    return float(-1)


def acts_for_limit_orders(df_ask, book_type, message_file, shares,start_time, end_time):
    '''
    if they're limiprevious_rowt orders, check market orders of excel in this period, sorted prices, update sizes accordingly,
    then update following rows which may be affected by the existence of my limit orders.
    :param df_ask: specifying ask book is just for reminding the difference of acts_for_market_orders.
    :param book_type:
    :param message_file:
    :param shares:
    :param start_time:
    :param end_time:
    :return: dataframes_update: only contains parts that after all market orders from excel of this time period.
    :return: price_size_list: list of [price,changed size] in thid time period
    '''
    # check the existence of market_order
    # if exist, get the time point, start ordering the price
    df_cancel_order_ask = message_file[2]
    # print("message file")
    # print(len(message_file))
    if book_type=="ask":
        message_file=message_file[1]
    else:
        message_file = message_file[0]
    market_order=check_market_order_exsitence(message_file,start_time,end_time)
    price_size_list=[]
    if len(market_order)>=1:
        for i in range(0,len(market_order)):
            # print("df_ask")
            # print(df_ask.head(5))
            # print("time_point")
            # print(round(market_order.at[i,'Time'],5))
            df_ask_before_time_point=df_ask[round(df_ask['Time'],5)<round(market_order.at[i,'Time'],5)]
            start_row_of_time_point=df_ask_before_time_point[-1:]
            # print("start_row_of_time_point\n")
            # print(start_row_of_time_point)
            end_row_of_time_point = start_row_of_time_point
            price_list,price_name_list=order_the_price(start_row_of_time_point.iloc[0],book_type)
            total_size=market_order.at[i,'Size']
            original_my_order_size=float(end_row_of_time_point["my_size"])
            for price in price_name_list:
                if total_size>0:
                    if price != "my_price":
                        end_row_of_time_point[book_type+"_size_"+price[-1]]=\
                            float(end_row_of_time_point[book_type+"_size_"+price[-1]])-total_size
                        # Attention, only update this row, we need to update rows in the original file.
                        if float(end_row_of_time_point[book_type+"_size_"+price[-1]])< 0:
                            total_size=(-1)*float(end_row_of_time_point[book_type+"_size_"+price[-1]])
                            end_row_of_time_point[book_type+"_size_" + price[-1]]=0
                            end_row_of_time_point[price]=0
                        else:
                            total_size=0
                    else:
                        if total_size >= float(end_row_of_time_point["my_size"]):
                            total_size -= float(end_row_of_time_point["my_size"])
                            end_row_of_time_point["my_size"] = 0
                            #end_row_of_time_point['my_price'] = 0
                        else:
                            inventory_numer=int(total_size/shares)
                            total_size-=inventory_numer*shares
                            end_row_of_time_point["my_size"]=\
                                float(end_row_of_time_point["my_size"])-inventory_numer*shares
                else:
                    break
            if total_size>0:
                print("No available sizes in the limit order book. ")
                print("If there are changes for final total_size is greater than 0? which means market order is much higher\n")
                print(total_size)
            # if do, please reopen a new market order in the coming stage.
            price_size_list.append([float(df_ask_before_time_point[-1:].my_price),original_my_order_size-float(end_row_of_time_point["my_size"])])
            # if (time_step==3) &(i==len(market_order)-1):
            #     if end_time==35640:
            #         pass
            #     dataframes_update = update_following_rows(df_ask, book_type, end_row_of_time_point,
            #                                               round(market_order.at[i, 'Time'], 5), message_file,end_time)
            # else:
            #     dataframes_update=update_following_rows(df_ask,book_type,end_row_of_time_point,round(market_order.at[i,'Time'],5),message_file)
            dataframes_update = update_following_rows(df_ask, book_type, end_row_of_time_point,
                                                      round(market_order.at[i, 'Time'], 5), message_file,df_cancel_order_ask)
            pd.set_option('display.max_columns', None)
            df_ask=dataframes_update
    return df_ask,price_size_list


def compare_old_new_result(dataframes,new_result,time_point,df_cancel_order_ask):
    '''
    to compare results with and without my order after a certain market period from excel.
    if difference exists, update the order rows before next market order from excel.
    since next market order make decisions on sizes according to the prices rank of the row before it.
    :param dataframes:
    :param new_result:
    :param time_point:
    :return:
    '''
    order_need_deleted=[]
    insert_point=dataframes.index[dataframes['Time']==time_point]

    last_row=dataframes.loc[insert_point]
    pd.set_option('display.max_columns', None)
    row_raw_result=[]
    row_new_result=[]
    left_order=[]
    flag=0
    for i in range(1,6):
        row_new_result.append([float(new_result['ask_price_'+str(i)]),float(new_result['ask_size_'+str(i)])])
        row_raw_result.append([float(last_row['ask_price_'+str(i)]),float(last_row['ask_size_'+str(i)])])
    cancel_order = df_cancel_order_ask[
        # (df_cancel_order_ask['Time'] >= float(new_result['Time'])) &
        (df_cancel_order_ask['Time'] == time_point)]

    for i in range(0, len(cancel_order)):
        order_need_deleted.append([float(cancel_order[i:i+1].Price), float(cancel_order[i:i+1].Size)])
    if len(order_need_deleted) > 0:
        for delete_order in order_need_deleted:
            for new_order in row_new_result:
                if delete_order[0]==new_order[0]:
                    new_order[1]-=delete_order[1]
    if operator.eq(row_new_result,row_raw_result):
            return left_order
    else:
        for i in row_new_result:
            for j in row_raw_result:
                if j[0] == i[0]:
                    flag=1
                    left_order.append([i[0], (float(i[1]) - float(j[1]))])
                    break
                else:
                    continue
            if flag==0:
                left_order.append(i)
            flag=0
        real_left_order=[]
        for i in left_order:
            if i[1]!=float(0):
                real_left_order.append(i)
        print('left_order:')
        print(real_left_order)
        return real_left_order


def update_following_rows(dataframes,book_type,new_result,time_point,message_file,df_cancel_order_ask):
    '''
    used for update order book which may be affected by my orders.
    :param dataframes:
    :param book_type:
    :param new_result:
    :param time_point: time of last market order from excel
    :param message_file: message file filtered by event type and direction.
    :return: dataframes_update: only contain parts after last market order
    '''
    print("start updating following rows.\n")
    next_market_order_dataframe=message_file[round(message_file['Time'],5) > time_point]
    left_order=compare_old_new_result(dataframes,new_result,time_point,df_cancel_order_ask)
    if len(left_order)>0:
        print(dataframes[dataframes['Time']==time_point])
    update_time_point = next_market_order_dataframe.iloc[0]

    last_updated_df = dataframes[round(dataframes['Time'],5) < round(update_time_point['Time'],5)]
    last_updated_row = last_updated_df[-1:]
    dataframes_update_raw=dataframes[round(dataframes['Time'],5)>=round(update_time_point['Time'],5)]
    dataframes_update=pd.concat([last_updated_row, dataframes_update_raw])
    if len(left_order)>0:
        for price_size in left_order:

            if price_size[0] not in list(last_updated_row[[book_type+'_price_1', book_type+'_price_2', book_type+'_price_3',
                                                           book_type+'_price_4', book_type+'_price_5']].iloc[0]):
                price_list, price_name_list = order_the_price(last_updated_row.iloc[0], book_type, price_size[0])
                rank_of_price = price_list.index(price_size[0])+1
                if int(rank_of_price) > 5:
                    continue
                else:
                    if last_updated_row.columns[last_updated_row.isin([price_list[-1]]).any()].tolist()==['my_price']:
                        column_name=price_list[-2]
                    else:
                        column_name = price_list[-1]
                    position_of_insert=last_updated_row.columns[last_updated_row.isin([column_name]).any()].tolist()
                    position_of_insert=position_of_insert[0]
                    # new_row = last_updated_row.iloc[:, (2 * rank_of_price - 1):-2].shift(2, axis=1)
                    # new_row.iloc[:, 0] = price_size[0]
                    # new_row.iloc[:, 1] = price_size[1]
                    # last_updated_row = pd.concat([last_updated_row.iloc[:, :2 * rank_of_price - 1], new_row, last_updated_row.iloc[:, -2:]],axis=1)
                    last_updated_row[position_of_insert]=price_size[0]
                    last_updated_row[position_of_insert[:4]+'size'+position_of_insert[-2:]]=price_size[1]
            else:
                insert_position=last_updated_row.iloc[0][lambda x: x == price_size[0]].index.tolist()
                last_updated_row[insert_position[0][:4]+'size'+insert_position[0][-2:]]\
                    =float(last_updated_row[insert_position[0][:4]+'size'+insert_position[0][-2:]])+price_size[1]

        dataframes_update[dataframes_update['Time']==float(last_updated_row['Time'])]=last_updated_row
    print("Updating ends\n.")
    # following_price=[]
    following_price=(float((new_result[0:1].my_size)))
    df_new=deepcopy(dataframes_update.loc[:,'my_size'])
    df_new['my_size']=following_price
    dataframes_update['my_size']=df_new['my_size']
    return dataframes_update


def acts_for_market_orders(df_bid, book_type, my_size, end_time):
    '''
    if my orders are market orders, find the last row of order book during this time period, deal my order with prices
    from this row, return sizes and prices.
    Attention: when selling, we need to check the final second of the bid book instead of ask book.
    :param df_bid: bid book of a certain time period we need when acts_for_limit_orders is ask.
    specifying bid book is just for reminding the difference of acts_for_limit_orders.
    :return: list of [price, changed size] for completing all of market orders we make.
    '''
    df_bid=df_bid[df_bid['Time'] <= end_time]
    final_row = df_bid[-1:]
    final_row['my_size']= my_size
    sorted_price,price_name_list = order_the_price(final_row.iloc[0], book_type)
    rest_my_order=float(final_row['my_size'])
    price_size_list=[]
    i=0
    for price_name_list in price_name_list:
        column_name=price_name_list
        copy_rest_my_order=rest_my_order
        rest_my_order-=float(final_row[column_name[:4]+'size'+column_name[-2:]])
        if rest_my_order<0:
            price_size_list.append([float(final_row[column_name]),copy_rest_my_order])
            return price_size_list
        else:
            price_size_list.append([float(final_row[column_name]),float(final_row[column_name[:4]+'size'+column_name[-2:]])])
        i=i+1
    if rest_my_order>0:
        print("Unusual case appears, rest of my order is too much.")
        return price_size_list.append(['error',rest_my_order])


if __name__=='__main__':
    message_file, dataframes= message_file_sum('CZR_2019-02-28_34200000_57600000_message_5.csv')
    certain_row = message_file[message_file.Time==34200.000]
    following_row = message_file[message_file.Time > 34200]
    certain_row = following_row.iloc[1:3]
    print(certain_row)
    time_point=34230
    dataframes_1 = dataframes[dataframes['Time'] < time_point]
    if str(time_point) not in dataframes['Time']:
        insert_point = len(dataframes_1)
        new_row = dataframes_1[-1:]
        # empty dataframes_1
        new_row.loc[0,'Time'] = time_point
        new_row.loc[0,'my_price'] = 400
        new_row.loc[0,'my_size'] = 1
        new_follow_row = dataframes[insert_point:]
        new_follow_row.loc[0,'my_price'] = 400
        new_follow_row.loc[0,'my_size'] = 1
        new_df = pd.concat([dataframes_1, new_row, new_follow_row]).reset_index(drop=True)
    else:
        new_df = dataframes[dataframes['Time'] >= time_point]
        new_df.loc[0,'my_price'] = 400
        new_df.loc[0,'my_size'] = 1
    print(new_df)


